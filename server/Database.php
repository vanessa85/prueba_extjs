<?php
class Database {
  private $_connection;
  private static $_instance;

  public static function getInstance() {
    if(!self::$_instance) {
      self::$_instance = new self();
    }
    return self::$_instance;
  }

  private function __construct() {
    $this->_connection = @pg_connect("host=".HOST." dbname=".DBNAME." user=".USER." password=".PASSWORD);
  }

  private function __clone() {}

  /*public function getConnection() {
    return $this->$_connection;
  }*/

  public function query($sql) {
    $result = pg_query($this->_connection, $sql) or die(pg_last_error());
    $data = [];

    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {
      $data[] = $row;
    }

    pg_free_result($result);
    return $data;
  }

  public function query_params($sql, $params) {
    $result = pg_query_params($this->_connection, $sql, $params) or die(pg_last_error());
    return $result;
  }

  public function close() {
    pg_close($this->_connection);
  }
}

?>
