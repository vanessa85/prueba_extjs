<?php
  require_once('config.php');
  require_once('Database.php');

  $request = $_REQUEST['request'];
  $db = Database::getInstance();

  switch ($request) {
    case 'trabajadores':
      $data = $db->query('SELECT * FROM prueba.trabajador WHERE est_ado = 1');
      $result = [
        'success' => true,
        'trabajadores' => $data
      ];
      response($result);
      break;

    case 'trabajadores-add':
      //$estado = isset($_POST['est_ado']) ? 1 : 0;
      $params = array($_POST['tra_cod'], $_POST['tra_nom'], $_POST['tra_pat'], $_POST['tra_mat']);
      //$params = array(300,'Javier','Perez','Robles',1);
      $query = "INSERT INTO prueba.trabajador (tra_cod,tra_nom,tra_pat,tra_mat,est_ado) VALUES ($1,$2,$3,$4)";
      $result = $db->query_params($query, $params);
      response(array('success'=> true, 'message' => 'added to the database'));
      break;

    case 'trabajadores-edit':
      //$estado = isset($_POST['est_ado']) ? 1 : 0;
      $params = array($_POST['tra_cod'], $_POST['tra_nom'], $_POST['tra_pat'], $_POST['tra_mat'], $_POST['tra_ide']);
      $query = "UPDATE prueba.trabajador SET tra_cod=$1, tra_nom=$2, tra_pat=$3, tra_mat=$4 WHERE tra_ide=$6";
      $result = $db->query_params($query, $params);
      response(array('success'=> true, 'message' => 'updated database'));
      break;

    case 'trabajadores-delete':
      $params = array($_POST['id']);
      $query = "UPDATE prueba.trabajador SET est_ado=0 WHERE tra_ide=$1";
      $result = $db->query_params($query, $params);
      response(array('success'=> true, 'message' => 'delete record of database'));
      break;

    case 'ventas':
      $data = [];
      $ventas = $db->query('SELECT * FROM prueba.venta WHERE est_ado=1');
      $i = 0;
      foreach ($ventas as $item) {
        $venta_id = $item['ven_ide'];
        $details = $db->query("SELECT * FROM prueba.venta_detalle WHERE ven_ide = $venta_id");
        $data[$i] = $item;
        $data[$i]['details'] = $details;
        $i++;
      }

      $result = [
        'success' => true,
        'ventas' => $data
      ];
      response($result);
      break;

    case 'ventas-add':
      $data = json_decode($_POST['data'], true);
      $items = getItems($data['items']);

      $query = "SELECT prueba.sp_insert_ventas($1, $2, $3, $4, $5)";
      $params = array($data['ven_ser'], $data['ven_num'], $data['ven_cli'], $data['ven_mon'], $items);
      $result = $db->query_params($query, $params);
      response(array('success'=>true, 'data'=>'inserted to the database'));
      break;

    case 'ventas-edit':
      $data = json_decode($_POST['data'], true);
      $items = getItems($data['items']);

      $query = "SELECT prueba.sp_update_ventas($1, $2, $3, $4, $5, $6)";
      $params = array($data['ven_ide'], $data['ven_ser'], $data['ven_num'], $data['ven_cli'], $data['ven_mon'], $items);
      $result = $db->query_params($query, $params);
      response(array('success'=>true, 'data'=>'updated to the database'));
      break;

    case 'ventas-delete':
      $params = array($_POST['id']);
      $query = "UPDATE prueba.venta SET est_ado=0 WHERE ven_ide=$1";
      $result = $db->query_params($query, $params);
      response(array('success'=> true, 'message' => 'delete record of database'));
    break;

    default:
      response('Invalid Method', 405);
      break;
  }

  $db->close();

  function response($data, $status = 200) {
    header("HTTP/1.1 " . $status . " " . requestStatus($status));
    header("Content-Type: application/json");
    echo json_encode($data);
    exit;
  }

  function requestStatus($code) {
    $status = array(
      200 => 'OK',
      404 => 'Not Found',
      405 => 'Method Not Allowed',
      500 => 'Internal Server Error',
    );
    return ($status[$code]) ? $status[$code] : $status[500];
   }

   function getItems($items) {
     $sales_details = '{';
     $i = 0;
     foreach ($items as $vd) {
       $sales_details .='"('.$vd['v_d_ide'].', 0, '.$vd['v_d_pro'].','.$vd['v_d_uni'].','.$vd['v_d_can'].','.$vd['v_d_tot'].', 0)"';
       $i++;
       if($i < count($items)) {
         $sales_details .= ',';
       }
     }
     $sales_details .= '}';

     return $sales_details;
   }

?>
