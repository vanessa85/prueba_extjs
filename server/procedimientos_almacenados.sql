CREATE OR REPLACE FUNCTION prueba.sp_insert_ventas(serie varchar,
numero varchar, cliente varchar, monto numeric, detalles prueba.venta_detalle[]) RETURNS void AS $$
DECLARE
	lastId bigint;
BEGIN
	INSERT INTO prueba.venta(ven_ser, ven_num, ven_cli, ven_mon)
	VALUES(serie, numero, cliente, monto);

	lastId:=LASTVAL();

	FOR i IN 1..array_length(detalles, 1) LOOP
		INSERT INTO prueba.venta_detalle(ven_ide, v_d_pro, v_d_uni, v_d_can)
		VALUES(lastId, detalles[i].v_d_pro, detalles[i].v_d_uni, detalles[i].v_d_can);
	END LOOP;

END;
$$ language 'plpgsql'

--procedimiento para actualizar ventas
CREATE OR REPLACE FUNCTION prueba.sp_update_ventas(id bigint, serie varchar,
numero varchar, cliente varchar, monto numeric, detalles prueba.venta_detalle[]) RETURNS void AS $$
BEGIN
	UPDATE prueba.venta SET ven_ser = serie, ven_num = numero, ven_cli = cliente, ven_mon = monto
	WHERE ven_ide = id;

	FOR i IN 1..array_length(detalles, 1) LOOP
		IF detalles[i].v_d_ide <> 0 THEN
			UPDATE prueba.venta_detalle SET v_d_pro = detalles[i].v_d_pro, v_d_uni = detalles[i].v_d_uni, v_d_can = detalles[i].v_d_can
			WHERE v_d_ide = detalles[i].v_d_ide;
		ELSE
			INSERT INTO prueba.venta_detalle(ven_ide, v_d_pro, v_d_uni, v_d_can)
			VALUES(id, detalles[i].v_d_pro, detalles[i].v_d_uni, detalles[i].v_d_can);
		END IF;
	END LOOP;

END;

$$ language 'plpgsql'
-- trigger para actualizar campo v_d_tot
CREATE OR REPLACE FUNCTION prueba.actualizar_total_vd() RETURNS TRIGGER
AS $tg_actualizar_total_vd$
DECLARE
BEGIN
	NEW.v_d_tot:= NEW.v_d_uni*NEW.v_d_can;
	RETURN NEW;
END;
$tg_actualizar_total_vd$
LANGUAGE 'plpgsql';

CREATE TRIGGER tg_actualizar_total_vd BEFORE INSERT OR UPDATE
ON prueba.venta_detalle FOR EACH ROW
EXECUTE PROCEDURE prueba.actualizar_total_vd();
