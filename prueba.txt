create schema prueba;
create table prueba.trabajador(
 tra_ide serial primary key,
 tra_cod integer default 0,
 tra_nom varchar(200) default '',
 tra_pat varchar(200) default '',
 tra_mat varchar(200) default '',
 est_ado integer default 1
);

Crear un formulario, que contenga un grid para mostrar los trabajadores, con botones para nuevo (Abre el panel con los campos en blanco para insertar), modificar (Abre el panel con la informacion del grid, para ser modificada),
eliminar (Cambia el estado del trabajador a 1),
un panel con los campos, tra_cod (codigo), tra_nom (nombre), tra_pat (apellido paterno), tra_mat (apellido materno)


Modulo Cabecera, detalle

create table prueba.venta(
  ven_ide serial primary key,
  ven_ser varchar(5) default '',
  ven_num varchar(100) default '',
  ven_cli text default '',
  ven_mon numeric (14,2)
);

create table prueba.venta_detalle(
  v_d_ide serial primary key,
  ven_ide integer,
  v_d_pro text default '',
  v_d_uni numeric(14,2) default 0.00,
  v_d_can numeric(14,2) default 0.00,
  v_d_tot numeric(14,2) default 0.00,
  est_ado integer default 1
)

Crear un formulario, en donde tenga 2 paneles, dentro del panel1 2 grid
gridcabecera, donde va a mostrar la informacion de venta y griddetalle en donde por cada venta seleccionada, va a imprimir el detalle de la venta.

Botones
Nuevo:  Abre el panel2, donde se coloca en la parte superior los campos para la cabecera de la venta y en la parte inferior para insertar los detalles d la venta.
Modificar: permite modificar los valores ingresados

Eliminar: coloca en est_ado = 0 a la venta

Mediante trigger, cuando se guarde el detalle de la venta, se debe actualizar el campo v_d_tot el cual es la multiplicacion de la cantidad * precio unitario (v_d_can * v_d_uni);
