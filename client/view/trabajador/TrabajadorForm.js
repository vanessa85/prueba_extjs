Ext.define('client.view.trabajador.TrabajadorForm', {
  extend: 'Ext.form.Panel',
  frame: true,
  //url: 'api/v1/trabajadores-add',
  alias: 'widget.trabajadorform',
  title: 'Formulario',
  border: false,
  bodyPadding: 10,
  defaults: { xtype: "textfield", allowBlank: false, msgTarget: 'side', anchor: '100%'},
  items: [
    {fieldLabel: 'Id', name: 'tra_ide', hidden: true, allowBlank: true},
    {fieldLabel: 'Código', name: 'tra_cod'},
    {fieldLabel: 'Nombres', name: 'tra_nom'},
    {fieldLabel: 'Apellido Paterno', name: 'tra_pat'},
    {fieldLabel: 'Apellido Materno', name: 'tra_mat'}
    //{fieldLabel: 'Estado', name: 'est_ado', xtype: 'checkboxfield', checked: true}
  ],
  buttons: [
    {
      text: 'Guardar',
      formBind: true,
      handler: function() {
        var that = this;
        var form = this.up('form').getForm();
        if (form.isValid()) {
          var panel = this.up("panel[codeName='panelMain']");

          form.submit({
            url: panel.getUrl(),
            waitMsg: 'Saving Data...',
            success: function(form, action) {
              var grid = panel.down("*[itemId='workerGrid']");
              grid.getStore().load();
              that.up('form').hide();
              grid.show();
            },
            failure: function(form, action) {
              console.log('action failure', action);
            }
          });
        }
      }
    },
    {
      text: 'Cancelar',
      handler: function() {
        var form = this.up('form');
        var panel = this.up("panel[codeName='panelMain']");
        var grid = panel.down("*[itemId='workerGrid']");
        form.hide();
        grid.show();
      }
    }
  ]
});
