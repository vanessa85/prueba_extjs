Ext.define('client.view.trabajador.TrabajadorGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.trabajadorgrid',
  store: Ext.create('Ext.data.Store', {
    fields: [
      {name: 'tra_ide', type: 'int'},
      {name: 'tra_cod', type: 'int'},
      {name: 'tra_nom', type: 'string'},
      {name: 'tra_pat', type: 'string'},
      {name: 'tra_mat', type: 'string'},
      {name: 'est_ado', type: 'boolean'}
    ],
    autoLoad: true,
    sorters: ['tra_nom', 'tra_pat', 'tra_mat'],
    proxy: {
      type: 'ajax',
      url: 'api/v1/trabajadores',
      reader: {
        type: 'json',
        root: 'trabajadores',
        successProperty: 'true'
      }
    }
  }),
  height: 500,
  viewConfig: {
    stripeRows: true,
    emptyText: 'No hay data que mostrar',
    //forceFit: true
  },
  //split: true,
  columns: [
    {text: 'Id', dataIndex: 'tra_ide', hidden: true, hideable: false},
    {text: 'Código', dataIndex: 'tra_cod', width: 100},
    {text: 'Nombres', dataIndex: 'tra_nom', flex: 1},
    {text: 'Apellido Paterno', dataIndex: 'tra_pat', flex: 1},
    {text: 'Apellido Materno', dataIndex: 'tra_mat', flex: 1},
    //{text: 'Estado', dataIndex: 'est_ado', width: 100, renderer: renderEstado}
  ],
  // listeners: {
  //   itemclick: function(dv, record, item, index, e) {
  //     //var selectedRec = dv.getSelectionModel().getSelected();
  //     console.log(index, record.get('tra_nom'));
  //   }
  // }
});

function renderEstado(value) {
  var color = value === true? "green": "red";
  var text = value === true? "Activo": "No activo";

  return Ext.String.format('<div style="color:{0}">{1}</div>', color, text);
}
