Ext.define('client.view.trabajador.TrabajadorScreen', {
  extend: 'Ext.panel.Panel',
  title: 'Trabajadores',
  alias: 'widget.trabajador',
  layout: {
    type: 'vbox',
    align: 'stretch'
  },
  // override initComponent
  initComponent: function() {
    //var that = this;
    this.tbar = [
        { text: 'Nuevo',
          //scope: that,
          itemId: 'btnNew',
          handler: this.onClickButton.bind(this, 'new')
        },
        { text: 'Editar',
          disabled: true,
          itemId: 'btnEdit',
          handler: this.onClickButton.bind(this, 'edit')
        },
        { text: 'Eliminar',
          disabled: true,
          itemId: 'btnDelete',
          handler: this.onClickButton.bind(this, 'delete')
        }
      ];

    this.callParent();

    var workerForm = this.getComponent('workerForm');
    workerForm.hide();
  },
  // override initEvents
  initEvents: function() {
    // call the superclass's initEvents implementation
    this.callParent();
    var workerGrid = this.getComponent('workerGrid');
    workerGrid.on('itemclick', this.onRowSelect, this);

  },
  items: [
    { xtype: 'trabajadorgrid',
      title: 'Listado de trabajadores',
      itemId: 'workerGrid'
    },
    { xtype: 'trabajadorform',
      itemId: 'workerForm'
    }
  ],
  getUrl: function() {
    return this.url;
  },
  setUrl: function(value) {
    this.url = value;
  },
  onRowSelect: function(dv, record, item, index, e) {
    this.getComponent('workerForm').loadRecord(record);
    var toolbar = this.getDockedItems('toolbar[dock="top"]')[0];
    toolbar.getComponent('btnEdit').setDisabled(false);
    toolbar.getComponent('btnDelete').setDisabled(false);
    this.itemSelected = record;
  },
  onClickButton: function(action) {
    var that = this;
    var grid = this.getComponent('workerGrid');
    var form = this.getComponent('workerForm');

    switch (action) {
      case 'new':
        grid.hide();
        form.show();
        form.getForm().reset();
        this.setUrl('api/v1/trabajadores-add');
        break;

      case 'edit':
        grid.hide();
        form.show();
        this.setUrl('api/v1/trabajadores-edit');
        break;

      case 'delete':
        Ext.Ajax.request({
          url: 'api/v1/trabajadores-delete',
          params: {id: this.itemSelected.get('tra_ide') },
          success: function(xhr) {
            console.log(xhr.responseText);
            grid.getStore().load();
          },
          failure: function(xhr) {
            console.log(`Error: ${xhr.statusText}`);
          }
        });
        break;
    }
  }
});
