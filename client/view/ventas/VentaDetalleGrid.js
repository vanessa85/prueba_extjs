Ext.define('client.view.ventas.VentaDetalleGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.ventadetallegrid',
  store: Ext.create('Ext.data.Store', {
    fields: [
      {name: 'v_d_ide', type: 'int'},
      {name: 'ven_ide', type: 'int'},
      {name: 'v_d_pro', type: 'string'},
      {name: 'v_d_uni', type: 'float'},
      {name: 'v_d_can', type: 'float'},
      {name: 'v_d_tot', type: 'float'}
    ]
  }),
  height: 100,
  viewConfig: {
    stripeRows: true,
    emptyText: 'No hay data que mostrar',
  },
  columns: [
    {text: 'Producto', dataIndex: 'v_d_pro', flex: 1, editor: {xtype: "textfield"}},
    {text: 'Unidades', dataIndex: 'v_d_uni', flex: 1, editor: {xtype: "textfield"}},
    {text: 'Cantidad', dataIndex: 'v_d_can', flex: 1, editor: {xtype: "textfield"}},
    {text: 'Total', dataIndex: 'v_d_tot', flex: 1}
  ]
});
