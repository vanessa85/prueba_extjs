Ext.define('client.view.ventas.VentaGrid', {
  extend: 'Ext.grid.Panel',
  alias: 'widget.ventagrid',
  store: Ext.create('Ext.data.Store', {
    fields: [
      {name: 'ven_ide', type: 'int'},
      {name: 'ven_ser', type: 'string'},
      {name: 'ven_num', type: 'string'},
      {name: 'ven_cli', type: 'string'},
      {name: 'ven_mon', type: 'float'},
      {name: 'details'}
    ],
    autoLoad: true,
    proxy: {
      type: 'ajax',
      url: 'api/v1/ventas',
      reader: {
        type: 'json',
        root: 'ventas',
        successProperty: 'true'
      }
    }
  }),
  height: 400,
  viewConfig: {
    stripeRows: true,
    emptyText: 'No hay data que mostrar',
  },
  columns: [
    {text: 'Código', dataIndex: 'ven_ser', width: 100},
    {text: 'Numero', dataIndex: 'ven_num', flex: 1},
    {text: 'Cliente', dataIndex: 'ven_cli', flex: 1},
    {text: 'Monto', dataIndex: 'ven_mon', flex: 1}
  ]
});
