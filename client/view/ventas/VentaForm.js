Ext.define('client.view.ventas.VentaForm', {
  extend: 'Ext.form.Panel',
  frame: true,
  alias: 'widget.ventaform',
  title: 'Formulario',
  border: false,
  bodyPadding: 10,
  initComponent: function() {
    this.items = this.buildItems();
    this.buttons = this.buildButtons();
    this.callParent();
  },
  buildItems: function() {
    return [{
        xtype: 'fieldset',
        title: 'Master',
        defaults: { xtype: "textfield", allowBlank: false, msgTarget: 'side', anchor: '100%', labelAlign: 'right', labelWidth: 100},
        items: [
          {fieldLabel: 'Id', name: 'ven_ide', hidden: true, allowBlank: true},
          {fieldLabel: 'Serie', name: 'ven_ser'},
          {fieldLabel: 'Numero', name: 'ven_num'},
          {fieldLabel: 'Cliente', name: 'ven_cli'},
          {fieldLabel: 'Monto', name: 'ven_mon'}
        ]
      },
      {
        xtype: 'fieldset',
        title: 'Details',
        layout: 'hbox',
        defaults: {xtype: 'textfield', labelWidth: 100, labelAlign: 'right'},
        items: [
          {fieldLabel: 'Id', name: 'v_d_ide', hidden: true},
          {fieldLabel: 'Producto', name: 'v_d_pro', flex: 1},
          {fieldLabel: 'Precio Unitario', name: 'v_d_uni', flex: 1},
          {fieldLabel: 'Cantidad', name: 'v_d_can', flex: 1},
          {xtype: 'button', text: 'Agregar', scope: this, handler: this.btnAddItem}
        ]
      },
      { xtype: 'ventadetallegrid',
        itemId: 'salesDetailsGrid',
        selType: 'rowmodel',
        plugins: [{
            ptype: 'rowediting',
            clickToEdit: 2
          }
        ]
      }
    ];
  },
  buildButtons: function() {
      return [
        {
          text: 'Guardar',
          formBind: true,
          scope: this,
          handler: this.btnSaveForm
        },
        {
          text: 'Cancelar',
          handler: function() {
            var panelVentas = this.up("panel[code='panelVentas']");
            var panelGrid = panelVentas.down("*[itemId='salesPanelGrid']");
            var panelForm = panelVentas.down("*[itemId='salesPanelForm']");
            panelGrid.show();
            panelForm.hide();
          }
        }
      ]
  },
  btnAddItem: function() {
    var grid = this.getComponent('salesDetailsGrid');
    var store = grid.getStore();
    var form = this.getForm();
    var producto = form.findField('v_d_pro').getValue().trim();
    var precio = form.findField('v_d_uni').getValue().trim();
    var cantidad = form.findField('v_d_can').getValue().trim();

    if(producto && precio && cantidad) {
      var total = Number(precio)*Number(cantidad);
      store.on('datachanged', function(src) {
        form.setValues({
          'v_d_pro': '',
          'v_d_uni': '',
          'v_d_can': ''
        });
      });
      store.add({v_d_ide: 0, ven_ide: 0, v_d_pro: producto, v_d_uni: precio, v_d_can: cantidad, v_d_tot: total});
    } else {
      Ext.MessageBox.show({
           title: 'Error',
           msg: 'Los campos no pueden estar vacios',
           buttons: Ext.MessageBox.OK,
           icon: Ext.MessageBox.ERROR
       });
    }
  },
  btnSaveForm: function() {
    var form = this.getForm();
    var panelVentas = this.up("panel[code='panelVentas']");

    if(form.isValid()) {
      var storeGrid = this.getComponent('salesDetailsGrid').getStore();
      var items = [];
      storeGrid.each(function(item) {
        items.push(item.data);
      });

      var values = form.getValues();
      var params = {};
      params.ven_ide = values.ven_ide;
      params.ven_mon = values.ven_mon;
      params.ven_num = values.ven_num;
      params.ven_ser = values.ven_ser;
      params.ven_cli = values.ven_cli;
      params.items = items;

      Ext.Ajax.request({
        url: panelVentas.getUrl(),
        method: 'POST',
        waitMsg: 'Saving Data...',
        params: {data: Ext.JSON.encode(params)},
        //jsonData: params,
        success: function(response) {
          var panelGrid = panelVentas.down("*[itemId='salesPanelGrid']");
          var panelForm = panelVentas.down("*[itemId='salesPanelForm']");
          panelGrid.getComponent('salesGrid').getStore().load();
          //console.log(panelGrid.getComponent('salesDetailsGrid').getStore())
          panelGrid.getComponent('salesDetailsGrid').getStore().removeAll();
          panelGrid.show();
          panelForm.hide();
        },
        failure: function(response) {
          console.log('failure')
        }
      });

    } else {
      console.log('no valido')
    }

  }
});
