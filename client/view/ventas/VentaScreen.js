Ext.define('client.view.ventas.VentaScreen', {
  extend: 'Ext.panel.Panel',
  title: 'Ventas',
  alias: 'widget.ventas',
  layout: {
    type: 'vbox',
    align: 'stretch'
  },
  // override initComponent
  initComponent : function() {
    this.tbar = [
      { text: 'Nuevo',
        itemId: 'btnNew',
        handler: this.onClickButton.bind(this, 'new')
      },
      { text: 'Editar',
        disabled: true,
        itemId: 'btnEdit',
        handler: this.onClickButton.bind(this, 'edit')
      },
      { text: 'Eliminar',
        disabled: true,
        itemId: 'btnDelete',
        handler: this.onClickButton.bind(this, 'delete')
      }
    ];

    this.callParent();

    var salesPanelForm = this.getComponent('salesPanelForm');
    salesPanelForm.hide();
  },
  // override initEvents
  initEvents: function() {
    // call the superclass's initEvents implementation
    this.callParent();
    var panelGrid = this.getComponent('salesPanelGrid');
    var salesGrid = panelGrid.getComponent('salesGrid');
    salesGrid.on('itemclick', this.onRowSelect, this);
  },
  items: [
    {
      xtype: 'panel',
      itemId: 'salesPanelGrid',
      items: [
        {
          xtype: 'ventagrid',
          title: 'Listado de Ventas',
          itemId: 'salesGrid'
        },
        {
          xtype: 'ventadetallegrid',
          title: 'Detalle de Ventas',
          itemId: 'salesDetailsGrid'
        }
      ]
    },
    {
      xtype: 'panel',
      itemId: 'salesPanelForm',
      items: [
        {
          xtype: 'ventaform',
          itemId: 'salesForm'
        }
      ]
    }
  ],
  getUrl: function() {
    return this.url;
  },
  onRowSelect: function(dv, record, item, index, e) {
    var panelGrid = this.getComponent('salesPanelGrid');
    var salesDetailsGrid = panelGrid.getComponent('salesDetailsGrid');
    salesDetailsGrid.getStore().loadData(record.get('details'));

    var toolbar = this.getDockedItems('toolbar[dock="top"]')[0];
    toolbar.getComponent('btnEdit').setDisabled(false);
    toolbar.getComponent('btnDelete').setDisabled(false);

    // var salesPanelForm = this.getComponent('salesPanelForm');
    // salesPanelForm.getComponent('salesForm').loadRecord(record);

    this.itemSelected = record;
  },
  onClickButton: function(action) {
    var panelGrid = this.getComponent('salesPanelGrid');
    var salesPanelForm = this.getComponent('salesPanelForm');
    var form = salesPanelForm.getComponent('salesForm').getForm();

    switch (action) {
      case 'new':
        this.url = 'api/v1/ventas-add';
        panelGrid.hide();
        salesPanelForm.show();
        form.reset();
        // var form = Ext.create('client.view.ventas.VentaForm');
        // this.add(form);
        // this.doLayout();
        break;

      case 'edit':
        this.url = 'api/v1/ventas-edit';
        panelGrid.hide();
        salesPanelForm.getComponent('salesForm').loadRecord(this.itemSelected);
        salesPanelForm.getComponent('salesForm').getComponent('salesDetailsGrid').getStore().loadData(this.itemSelected.get('details'));
        salesPanelForm.show();
        break;

      case 'delete':
        Ext.Ajax.request({
          url: 'api/v1/ventas-delete',
          params: {id: this.itemSelected.get('ven_ide') },
          success: function(xhr) {
            console.log(xhr.responseText);
            panelGrid.getComponent('salesGrid').getStore().load();
            panelGrid.getComponent('salesDetailsGrid').getStore().removeAll();
          },
          failure: function(xhr) {
            console.log(`Error: ${xhr.statusText}`);
          }
        });
        break;
    }
  }
});
