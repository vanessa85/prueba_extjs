Ext.define('client.store.Trabajador', {
  extend: 'Ext.data.Store',
  fields: [
    {name: 'tra_id', type: 'int'},
    {name: 'tra_cod', type: 'int'},
    {name: 'tra_nom', type: 'string'},
    {name: 'tra_pat', type: 'string'},
    {name: 'tra_mat', type: 'string'},
    {name: 'est_ado', type: 'boolean'}
  ],
  autoLoad: true,
  sorters: ['tra_nom', 'tra_pat', 'tra_mat'],
  proxy: {
    type: 'ajax',
    url: 'api/v1/trabajadores',
    reader: {
      type: 'json',
      root: 'trabajadores',
      successProperty: 'true'
    }
  }
});
