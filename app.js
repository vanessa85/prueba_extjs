Ext.Loader.setConfig({enabled: true});

Ext.require([
    'Ext.grid.*',
    'Ext.data.*',
    'Ext.panel.*',
    'Ext.form.*',
    //'client.store.Trabajador',
    'client.view.trabajador.TrabajadorForm',
    'client.view.trabajador.TrabajadorGrid',
    'client.view.trabajador.TrabajadorScreen',
    'client.view.ventas.VentaForm',
    'client.view.ventas.VentaGrid',
    'client.view.ventas.VentaDetalleGrid',
    'client.view.ventas.VentaScreen'
]);

Ext.onReady(function(){

  Ext.create('Ext.container.Viewport', {
    layout:'fit',
    items: [
      Ext.create('Ext.tab.Panel', {
        activeTab: 0,
        items: [
          {
            title: 'Tabajadores',
            bodyPadding: 10,
            codeName:'panelMain',
            xtype: 'trabajador'
          },
          {
            title: 'Ventas',
            bodyPadding: 10,
            xtype: 'ventas',
            code: 'panelVentas'
          }
        ]
      })
    ],
    renderTo: Ext.getBody()
  });

});
